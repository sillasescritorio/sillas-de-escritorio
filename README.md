Sillas Top es una web en la que se muestran las mejores sillas de oficina para todas las edades y necesidades. Dispone de sillas infantiles, sillas es escritorio y de oficina para adultos, todas ellas ergonómicas y fabricadas con la mejor calidad. Sus precios son muy asequibles y te las envían a tu domicilio.

Ejemplo:

Silla de oficina City 80. Características:

• Asiento con ajuste de altura Toplift
• Piel sintética de referencia
• Respaldo con reposacabezas integrados
• Mecanismo de inclinación ajustable al peso corporal
• Base de acero cromado
• Alto confort
• Doble freno de seguridad

www.sillastop.com